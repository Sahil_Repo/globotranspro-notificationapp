#!/usr/bin/python3
import sys, getopt
from flask import Flask, jsonify, request
from flask_restful import Api, Resource
from services.services import Endpoint as endpoint

"""
This is REST API service for GloboTrans Pro's Notification Service 
App Name:   GloboTrans Pro Notification Service
Author:     Sahil Shaikh
Date:       7th November 2021
"""
app = Flask(__name__)
api = Api(app)

def main(argv):
    type = ''
    try:
       opts, args = getopt.getopt(argv,"ht:",["help","type="])

    except getopt.GetoptError:
        print('app.py -type ( LEAVES | LEADSDSR | TASKALERT )')
        sys.exit(2)      

    for opt, arg in opts:
        if opt == '-h':
            print('app.py -type ( LEAVES | LEADSDSR | TASKALERT )')
            sys.exit()
        elif opt in ("-t", "--type"):
            type = arg
    print('type is', type)

    #run module based upon the selection arg
    if( type == "LEAVES" ):
        endpoint.leave_notification()
    elif( type == "LEADSDSR" ):
        endpoint.leaddsr_notification()
    elif( type == "TASKALERT" ):
        endpoint.task_notification()
    else:
        print("invalid arg. --type pass should match either LEAVES or LEADSDSR or TASKALERT")  

class Leave_SendReminder(Resource):
    def post(self):
        print('Helo')


# API Endpoints defined for notification service.
api.add_resource(Leave_SendReminder,"/leave/sendreminder")

# Run the Rest API on the port 8060. This application is using docker hence the host is assign as 0.0.0.0
if __name__ == "__main__":
    main(sys.argv[1:])
    # app.run(debug=True, host="0.0.0.", port=8060)