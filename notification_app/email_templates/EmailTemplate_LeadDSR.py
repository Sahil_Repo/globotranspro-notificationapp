import pandas as pd
from string import Template

# Create the plain-text and HTML version of your message    
text_body = """\
Hello,
Your Daily Status Report for Leads Management is Ready.

Unfortunately, your device doesn't support showing HTML Table in email. 
Please find attached excel sheet.

Thanks,
ERP GloboTranspro via (WEQ Softwares)
"""

html_body = """\
<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" />
    <title>Lead DSR Notification from ERP GloboTrans Pro</title>
</head>
<style>
    a {
        color: #1c1c00 !important;
    }

    a:hover {
        text-decoration: underline !important;
    }
</style>

<body marginheight="0" topmargin="0" marginwidth="0"
    style="margin: 0px; color:#4e4e4ede; font-size: 14px;font-weight:300; background-color: #f2f8f9;" leftmargin="0">
    <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f8f9"
        style=" font-family: 'Roboto', sans-serif , Arial, Helvetica, sans-serif;">
        <tr>
            <td>
                <table style="background-color: #f2f8f9; max-width:670px; margin:0 auto 5rem;" width="100%" border="0"
                    align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="height:30px;">&nbsp;</td>
                    </tr>
                    <!-- Logo -->
                    <tr>
                        <td style="text-align:center;">
                            <a href="https://erp.globotranspro.com" title="ERP GloboTrans Pro"><img
                                    src="http://globotranspro.com/wp/wp-content/uploads/2017/01/GloboTransPRO-Brand-1.png"
                                    style="width: 30%;" alt="ERP GloboTrans Pro"></a>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:30px;">&nbsp;</td>
                    </tr>
                    <!-- Email Content -->
                    <tr>
                        <td>

                            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
                                style="max-width:670px; background:#fff; border-radius:3px; -webkit-box-shadow:0 1px 3px 0 rgba(0, 0, 0, 0.16), 0 1px 3px 0 rgba(0, 0, 0, 0.12);-moz-box-shadow:0 1px 3px 0 rgba(0, 0, 0, 0.16), 0 1px 3px 0 rgba(0, 0, 0, 0.12);box-shadow:0 1px 3px 0 rgba(0, 0, 0, 0.16), 0 1px 3px 0 rgba(0, 0, 0, 0.12); padding: 40px;line-height: 1.5;border-top: 3px solid #1c1c00;">
                                <!-- Details Table -->
                                <tr>
                                    <td>
                                        <strong>Hello,</strong><br />
                                        <p></p>
                                        <table cellpadding="0" cellspacing="0" style="width: 100%;">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div>
                                                            <p style="margin: 5px 0;">Your Daily Status Report for Leads
                                                                Management is Ready. Please find attached excel sheet.
                                                            </p>
                                                            <table
                                                                style="border: 1px solid #ededed;border-spacing: 0;width: 100%; margin: 1rem 0;">
                                                                $LEAD_DSR
                                                            </table>

                                                        </div>
                                                        <p style="margin: 0;padding-top: 15px; font-weight: 600;">
                                                            Thanks,<br>ERP GloboTrans ProLogistix</p>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                            <p style="font-size:14px; color:#455056bd; line-height:18px; margin:0 0 0;">© 2021 WEQ
                                Technologies, All rights reserved.</p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>

</html>
"""

def create_email_body(inputdataframe):
  table_cols = [ "Client Name", "Project Name", "Lead Stage", "Lead Status", "Incharge", "Created By"]
  table_rows = "<tr>"
  for cols in table_cols:
    table_rows += """<th style="font-weight:500; color:#fff; font-size:16px; background-color:cornflowerblue;padding: 5px;border-right: 1px solid #ededed;border-bottom: 1px solid #ededed;">
    """
    table_rows += cols
    table_rows += "</th>"

  table_rows += "</tr>"
  
  td_tag = """
  <td style="padding: 5px;border-bottom: 1px solid #ededed;">
  """
  td_tag_red = """
  <td style="padding: 5px;border-bottom: 1px solid #ededed;color:#C32D39;">
  """
  td_tag_green = """
  <td style="padding: 5px;border-bottom: 1px solid #ededed;color:#009900;">
  """

  for index_counter in inputdataframe.index:
    table_rows += "<tr>"
    table_rows += td_tag + str( inputdataframe["client_name"][index_counter] ) + "</td>"
    table_rows += td_tag + str( inputdataframe["project_name"][index_counter] ) + "</td>"
      
    table_rows += td_tag + str( inputdataframe["lead_stage"][index_counter] ) + "</td>"

    lead_status = str( inputdataframe["lead_status_code"][index_counter] )
    if( lead_status == "Missed Opportunity" ):
        table_rows += td_tag_red + lead_status + "</td>"
    elif( lead_status == "Awarded" ):
        table_rows += td_tag_green + lead_status + "</td>"
    else:
        table_rows += td_tag + lead_status + "</td>"

    table_rows += td_tag + str( inputdataframe["lead_incharge_name"][index_counter] ) + "</td>"
    table_rows += td_tag + str( inputdataframe["creator_name"][index_counter] ) + "</td>"
    # table_rows += td_tag + str( inputdataframe["remarks"][index_counter] ) + "</td>"
    table_rows += "</tr>"

  # Substitute parameters with strings in html & plain text strings.
  html_body_template = Template( html_body )
  html_body_template = html_body_template.safe_substitute(LEAD_DSR=table_rows)
  return (text_body, html_body_template)