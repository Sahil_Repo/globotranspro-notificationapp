from string import Template
import timeago, datetime

# Create the plain-text and HTML version of your message
text_body = """\
Hello,

Task Notification from $ASSIGNEE_NAME; task created/modified $CREATED_AGO.
------------------------------------------
Task Date       : $TASK_DATE
Target Date     : $TARGET_DATE
Task Status     : $STATUS_CODE
Assigned To     : $ASSIGNEDTO_NAME

Task Details
***----------------***
$TASK_DESCRIPTION
------------------------------------------

Thanks,
ERP GloboTranspro via (WEQ Softwares)
"""

html_body = """\
<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" />
    <title>Task Notification from $ASSIGNEE_NAME</title>
</head>
<style>
    a {
        color: #1c1c00 !important;
    }

    a:hover {
        text-decoration: underline !important;
    }
</style>

<body marginheight="0" topmargin="0" marginwidth="0"
    style="margin: 0px; color:#4e4e4ede; font-size: 14px;font-weight:200; background-color: #f2f8f9;" leftmargin="0">
    <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f8f9"
        style=" font-family: 'Roboto', sans-serif , Arial, Helvetica, sans-serif;">
        <tr>
            <td>
                <table style="background-color: #f2f8f9; max-width:670px; margin:0 auto 5rem;" width="100%" border="0"
                    align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="height:30px;">&nbsp;</td>
                    </tr>
                    <!-- Logo -->
                    <tr>
                        <td style="text-align:center;">
                            <a href="https://erp.globotranspro.com" title="ERP GloboTrans Pro"><img
                                    src="http://globotranspro.com/wp/wp-content/uploads/2017/01/GloboTransPRO-Brand-1.png"
                                    style="width: 30%;" alt="ERP GloboTrans Pro"></a>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:30px;">&nbsp;</td>
                    </tr>
                    <!-- Email Content -->
                    <tr>
                        <td>

                            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
                                style="text-align: left; max-width:670px; background:#fff; border-radius:3px; -webkit-box-shadow:0 1px 3px 0 rgba(0, 0, 0, 0.16), 0 1px 3px 0 rgba(0, 0, 0, 0.12);-moz-box-shadow:0 1px 3px 0 rgba(0, 0, 0, 0.16), 0 1px 3px 0 rgba(0, 0, 0, 0.12);box-shadow:0 1px 3px 0 rgba(0, 0, 0, 0.16), 0 1px 3px 0 rgba(0, 0, 0, 0.12); padding: 40px;line-height: 1.5;border-top: 3px solid #1c1c00;">
                                <!-- Details Table -->
                                <tr>
                                    <td>
                                        <strong>Hello,</strong><br />
                                        <p></p>
                                        <table cellpadding="0" cellspacing="0" style="width: 100%;">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div>
                                                            <p style="margin: 5px 0;">
                                                            Task Notification from $ASSIGNEE_NAME; task created/modified $CREATED_AGO. Please Update the task status as soon as it is done. 
                                                            </p>
                                                            <table style="border: 1px solid #ededed;border-spacing: 0;width: 100%; margin: 1rem 0;">
                                                              <tr style="">
                                                                  <th style="padding: 5px;width: 30%;border-right: 1px solid #ededed;border-bottom: 1px solid #ededed;">Task Date</th>
                                                                  <td style="padding: 5px;border-bottom: 1px solid #ededed;">$TASK_DATE</td>
                                                              </tr>
                                                              <tr style="">
                                                                  <th style="padding: 5px;width: 30%;border-right: 1px solid #ededed;border-bottom: 1px solid #ededed;">Target Date</th>
                                                                  <td style="padding: 5px;border-bottom: 1px solid #ededed;">$TARGET_DATE</td>
                                                              </tr>
                                                              <tr style="">
                                                                  <th style="padding: 5px;width: 30%;border-right: 1px solid #ededed;border-bottom: 1px solid #ededed;">Task Status</th>
                                                                  <td style="padding: 5px;border-bottom: 1px solid #ededed;">$STATUS_CODE</td>
                                                              </tr>
                                                              <tr style="">
                                                                  <th style="padding: 5px;width: 30%;border-right: 1px solid #ededed;border-bottom: 1px solid #ededed;">Assigned To</th>
                                                                  <td style="padding: 5px;border-bottom: 1px solid #ededed;">$ASSIGNEDTO_NAME</td>
                                                              </tr>
                                                              <tr style="">
                                                                  <th style="padding: 5px;width: 30%;border-right: 1px solid #ededed;border-bottom: 1px solid #ededed;">Task Details</th>
                                                                  <td style="padding: 5px;border-bottom: 1px solid #ededed;">$TASK_DESCRIPTION</td>
                                                              </tr>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <p style="margin: 0;padding-top: 15px;">
                                            Thanks,<br> <b>ERP GloboTrans ProLogistix</b> </p>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                            <p style="font-size:14px; color:#455056bd; line-height:18px; margin:0 0 0;">© 2021 WEQ
                                Technologies, All rights reserved.</p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>

</html>
"""

def create_email_body(tb_task, indexcounter):
    print("Ago : ", tb_task["created_ago"][indexcounter]) 
    created_modified_ago = ( timeago.format(datetime.timedelta(minutes= tb_task["created_ago"][indexcounter])) )

    # Substitute parameters with strings in html & plain text strings.
    text_body_template = Template( text_body )
    text_body_template = text_body_template.safe_substitute(
        ASSIGNEE_NAME = tb_task["assignee_name"][indexcounter],
        CREATED_AGO = created_modified_ago,
        TASK_DATE = tb_task["task_date"][indexcounter].strftime("%d-%b-%y"),
        TARGET_DATE = tb_task["target_date"][indexcounter].strftime("%d-%b-%y"),
        STATUS_CODE = tb_task["status_code"][indexcounter],
        ASSIGNEDTO_NAME = tb_task["assignedto_name"][indexcounter],
        TASK_DESCRIPTION = tb_task["task_description"][indexcounter]
    )

    # Substitute parameters with strings in html & plain text strings.
    html_body_template = Template( html_body )
    html_body_template = html_body_template.safe_substitute(
        ASSIGNEE_NAME = tb_task["assignee_name"][indexcounter],
        CREATED_AGO = created_modified_ago,
        TASK_DATE = tb_task["task_date"][indexcounter].strftime("%d-%b-%y"),
        TARGET_DATE = tb_task["target_date"][indexcounter].strftime("%d-%b-%y"),
        STATUS_CODE = tb_task["status_code"][indexcounter],
        ASSIGNEDTO_NAME = tb_task["assignedto_name"][indexcounter],
        TASK_DESCRIPTION = tb_task["task_description"][indexcounter]
    )
    return (text_body_template, html_body_template)