#!/usr/bin/python3
from email import encoders
from email.mime.base import MIMEBase
from email.mime.application import MIMEApplication
import smtplib, ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import os
from dotenv import load_dotenv

class notificationservice:

    port = os.getenv('SMTP_PORT')  # For SSL
    smtp_server = os.getenv('SMTP_SERVER')
    sender_smtp_login = os.getenv('SENDER_SMTP_LOGIN')
    sender_smtp_pwd = os.getenv('SENDER_SMTP_PWD')

    def sendHTMLNotification(sender_email, receiver_emails, subject_line, plain_text, html_body, attached_file= "NA", fname ="NA"):
        message = MIMEMultipart("mixed")
        message["From"] = sender_email
        message["To"] = ','.join(receiver_emails)
        message["Subject"] = subject_line

        # Turn these into plain/html MIMEText objects
        part1 = MIMEText(plain_text,"plain")
        part2 = MIMEText(html_body,"html")

        # Add HTML/plain-text parts to MIMEMultipart message
        # The email client will try to render the last part first
        message.attach(part1)
        message.attach(part2)

        if( attached_file != "NA" ): 
            # Add attached file to Email Message
            with open(attached_file,"rb") as attachment:
                # Add file as application/octet-stream
                # Email client can usually download this automatically as attachment
                part3 = MIMEApplication("application", "octet-stream")
                part3.set_payload(attachment.read())

            # Encode file in ASCII characters to send by email    
            encoders.encode_base64(part3)
            # Add header as key/value pair to attachment part
            part3.add_header(
                "Content-Disposition",
                "attachment", 
                filename=fname
            )
            # Add attached part3 to MIMEMultipart message
            message.attach(part3)


        # Create a secure connection with server and send email
        ssl_cont = ssl.create_default_context()
        with smtplib.SMTP_SSL(notificationservice.smtp_server, notificationservice.port, context=ssl_cont) as server:
            server.login(notificationservice.sender_smtp_login, notificationservice.sender_smtp_pwd)
            server.sendmail(
                sender_email, receiver_emails, message.as_string()
            )