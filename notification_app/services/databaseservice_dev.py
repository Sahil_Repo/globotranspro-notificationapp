#!/usr/bin/python3
import pandas as pd


class DatabaseSets:

    # alert-for_employeeleaves Method return pandas.DataFrame for leaves applied or modified in past 1 hour.
    @staticmethod
    def alert_for_employeeleaves():
        tb_empleaves = pd.ExcelFile("emp_leaves_dataframe.xlsx")
        tb_empleaves = pd.read_excel(tb_empleaves, sheet_name="leave details")
        tb_empmaster = pd.ExcelFile("emp_master.xlsx")
        tb_empmaster = pd.read_excel(tb_empmaster,sheet_name="employeemaster")
        tb_predefinedmaster = pd.ExcelFile("predefined_master.xlsx")
        tb_predefinedmaster = pd.read_excel(tb_predefinedmaster, sheet_name="predefinedmaster")

       