#!/usr/bin/python3
import datetime
from datetime import timedelta
import pandas as pd
import sqlalchemy as sqlal
import os
from urllib.parse import quote
from dotenv import load_dotenv

load_dotenv()  # take environment variables from .env.

#Database connection details
Hostname= os.getenv('DB_HOSTNAME')
Username = os.getenv('DB_USERNAME')
Password= quote(os.getenv('DB_PASSWORD'))
DatabaseName = os.getenv('DB_DATABASENAME')

class DatabaseSets:

    # alert-for_employeeleaves Method return pandas.DataFrame for leaves applied or modified in past 1 hour.
    @staticmethod
    def alert_for_employeeleaves():
        engine = sqlal.create_engine("mysql+pymysql://"+ Username +":"+ Password +"@"+ Hostname +"/"+ DatabaseName)

        #create `employee_leave_detail_master` DataFrame from SQL Table
        tb_empleaves = pd.read_sql_table(
            "employee_leave_detail_master", 
            engine, 
            columns={"employee_name", "from_date", "to_date", "leave_type", "created_date", "modified_date", "purpose_of_leave", "status", "employee_id", "reporting_manager_id", "is_delete", "approval_one", "approval_two"}
            )
        #create `employee_master` DataFrame from SQL Table
        tb_empmaster = pd.read_sql_table(
            "employee_master", 
            engine, 
            columns={"employee_id", "employee_name", "employee_designation", "email_address"}
            )
        #create `predefined_master` DataFrame from SQL Table
        tb_predefinedmaster = pd.read_sql_table(
            "predefined_master",
            engine,
            columns={"predefined_id","name","sub_name"}
        )

        # calculate Created Ago & Modified Ago hours and add column to DataFrame
        tb_empleaves["created_ago"] = (datetime.datetime.now() - tb_empleaves["created_date"])/timedelta(hours=1)
        tb_empleaves["modified_ago"] = (datetime.datetime.now() - tb_empleaves["modified_date"])/timedelta(hours=1)
        
        # slice dataset by rows for created or modified within an hour
        tb_empleaves = tb_empleaves[ (tb_empleaves["created_ago"] <= 1) | (tb_empleaves["modified_ago"] <= 1) ]

        combined_data_sets = pd.merge(tb_empleaves, tb_empmaster[["employee_id","employee_name","employee_designation", "email_address"]], how="left", left_on="employee_id", right_on="employee_id")
        combined_data_sets = combined_data_sets.rename(columns={
            "employee_name_x":"employee_name", 
            "employee_name_y":"off_emp_name", 
            "employee_designation":"off_emp_designation",
            "email_address":"off_emp_email_address"
            })

        combined_data_sets = pd.merge(combined_data_sets, tb_empmaster[["employee_id","employee_name","employee_designation", "email_address"]], how="left", left_on="reporting_manager_id", right_on="employee_id")
        combined_data_sets = combined_data_sets.rename(columns={
            "employee_id_x":"employee_id", 
            "employee_id_y":"mgr_emp_id", 
            "employee_name_x":"employee_name", 
            "employee_name_y":"mgr_emp_name", 
            "employee_designation":"mgr_emp_designation",
            "email_address":"mgr_emp_email_address"
            })
        
        combined_data_sets = pd.merge(combined_data_sets, tb_predefinedmaster[["predefined_id", "name"]], how="left", left_on="leave_type", right_on="predefined_id")
        combined_data_sets = pd.merge(combined_data_sets, tb_predefinedmaster[["predefined_id", "name"]], how="left", left_on="status", right_on="predefined_id")
        #renaming few column for easy recognisation
        combined_data_sets = combined_data_sets.rename(columns={
            "name_y":"status_code", 
            "predefined_id_y":"status_code_pref_id",
            "name_x":"leave_code",
            "predefined_id_x":"leave_code_pref_id"
        })

        combined_data_sets = pd.merge(combined_data_sets, tb_predefinedmaster[["predefined_id", "name"]], how="left", left_on="approval_one", right_on="predefined_id")
        combined_data_sets = combined_data_sets.rename(columns={
            "name":"approval_one_name", 
            "predefined_id":"approval_one_pref_id", 
        })
        combined_data_sets = pd.merge(combined_data_sets, tb_predefinedmaster[["predefined_id", "name"]], how="left", left_on="approval_two", right_on="predefined_id")
        combined_data_sets = combined_data_sets.rename(columns={
            "name":"approval_two_name", 
            "predefined_id":"approval_two_pref_id", 
        })

        combined_data_sets = combined_data_sets.loc[ combined_data_sets["is_delete"] != 'Y', [
            "from_date", "to_date", "is_delete", "status_code", "leave_code", 
            "purpose_of_leave", "created_ago", "modified_ago",  "off_emp_name",
            "off_emp_designation", "off_emp_email_address", "employee_id", "mgr_emp_id", 
            "mgr_emp_name", "mgr_emp_designation", "mgr_emp_email_address",
            "approval_one_name", "approval_two_name"]]
        combined_data_sets = combined_data_sets.fillna(value={
            "mgr_emp_name":"NA",
            "status_code":"NA",
            "leave_code":"NA"
            })

        return combined_data_sets

    # alert_for_leadsDSR Method return pandas.DataFrame for past 3 months of 
    # Leads data created or modified and non-deleted ones.
    @staticmethod
    def alert_for_leadsDSR():
        engine = sqlal.create_engine("mysql+pymysql://"+ Username +":"+ Password +"@"+ Hostname +"/"+ DatabaseName)

        #create `lead_master` DataFrame from SQL Table
        tb_leadmaster = pd.read_sql_table(
            "lead_master", 
            engine
            )
        #create `employee_master` DataFrame from SQL Table
        tb_empmaster = pd.read_sql_table(
            "employee_master", 
            engine, 
            columns={"employee_id", "employee_name", "employee_designation", "email_address"}
            )
        #create `predefined_master` DataFrame from SQL Table
        tb_predefinedmaster = pd.read_sql_table(
            "predefined_master",
            engine,
            columns={"predefined_id","name","sub_name"}
        )

        
        tb_leadmaster["created_ago"] = (datetime.datetime.now() - tb_leadmaster["created_date"])/timedelta(hours=1)
        tb_leadmaster["modified_ago"] = (datetime.datetime.now() - tb_leadmaster["modified_date"])/timedelta(hours=1)

        # select rows in last 3 months & yes is_delete as 'N'
        tb_leadmaster = tb_leadmaster[ 
            (tb_leadmaster["is_delete"] == 'N') & ( 
                (tb_leadmaster["modified_ago"] < 2160.0) | 
                (tb_leadmaster["created_ago"] < 2160.0) 
                )
            ]
        
        # stage_id => merge
        tb_leadmaster = pd.merge( tb_leadmaster, tb_predefinedmaster[["predefined_id","name"]], how="left", left_on="stage_id", right_on="predefined_id")
        #renaming few column for easy recognisation
        tb_leadmaster = tb_leadmaster.rename(columns={"name":"lead_stage"})
        # drop duplicate columns
        tb_leadmaster =  tb_leadmaster.drop("predefined_id",1)

        # priority_id => merge
        tb_leadmaster = pd.merge( tb_leadmaster, tb_predefinedmaster[["predefined_id","name"]], how="left", left_on="priority_id", right_on="predefined_id")
        #renaming few column for easy recognisation
        tb_leadmaster = tb_leadmaster.rename(columns={"name":"priority_code"})
        # drop duplicate columns
        tb_leadmaster =  tb_leadmaster.drop("predefined_id",1)
        
        # type_id => merge
        tb_leadmaster = pd.merge( tb_leadmaster, tb_predefinedmaster[["predefined_id","name"]], how="left", left_on="type_id", right_on="predefined_id")
        #renaming few column for easy recognisation
        tb_leadmaster = tb_leadmaster.rename(columns={"name":"lead_type_code"})
        # drop duplicate columns
        tb_leadmaster =  tb_leadmaster.drop("predefined_id",1)

        # status => merge
        tb_leadmaster = pd.merge( tb_leadmaster, tb_predefinedmaster[["predefined_id","name"]], how="left", left_on="status", right_on="predefined_id")
        #renaming few column for easy recognisation
        tb_leadmaster = tb_leadmaster.rename(columns={"name":"lead_status_code"})
        # drop duplicate columns
        tb_leadmaster =  tb_leadmaster.drop("predefined_id",1)

        # lead_incharge_id => merge
        tb_leadmaster = pd.merge( tb_leadmaster, tb_empmaster[["employee_id","employee_name", "email_address"]], how="left", left_on="lead_incharge_id", right_on="employee_id")
        #renaming few column for easy recognisation
        tb_leadmaster = tb_leadmaster.rename(columns={
            "employee_name":"lead_incharge_name", 
            "employee_designation":"lead_incharge_desgn",
            "email_address":"lead_incharge_email_addr"
            })
       
        # drop duplicate columns
        tb_leadmaster =  tb_leadmaster.drop("employee_id",1)

        # created_by => emp_master
        tb_leadmaster = pd.merge( tb_leadmaster, tb_empmaster[["employee_id","employee_name", "email_address"]], how="left", left_on="created_by", right_on="employee_id")
        #renaming few column for easy recognisation
        tb_leadmaster = tb_leadmaster.rename(columns={
            "employee_name":"creator_name", 
            "employee_designation":"creator_desgn",
            "email_address":"creator_email_addr"
            })

        #sort by created_date latest first
        tb_leadmaster = tb_leadmaster.sort_values(by=["created_date","priority_code"], ascending=[False,False])

        # Replace empty cells with `-`
        tb_leadmaster = tb_leadmaster.fillna(value={
            "project_name":"-",
            "lead_stage":"-",
            "priority_code":"-",
            "lead_type_code":"-",
            "lead_incharge_name":"-",
            "creator_name":"-",
            "lead_status_code":"Not Defined"
             })

        tb_leadmaster = tb_leadmaster[[
            "lead_id",
            "lead_code", 
            "client_name",
            "project_name", 
            "lead_stage", 
            "lead_status_code",
            "priority_code", 
            "lead_type_code", 
            "comments",
            "lead_incharge_name", 
            "lead_incharge_email_addr",
            "creator_name",
            "creator_email_addr",
            "next_followup",
            "target_date",
            "created_date", 
            "modified_date",
            "revision",
            "contract_created"
            ]]

        # Renaming Certain column for good representation on Excel Output
        tb_leadmaster = tb_leadmaster.rename(columns={
            "created_date":"added_on", 
            "modified_date":"last_modified_on",
            "revision":"quote_revision_count",
            "comments":"remarks"
            })

        tb_leadmaster_xlsx = tb_leadmaster[[
            "lead_code", 
            "client_name",
            "project_name", 
            "priority_code",
            "lead_stage", 
            "lead_status_code",
            "lead_type_code", 
            "remarks",
            "target_date",
            "next_followup",
            "lead_incharge_name", 
            "creator_name",
            "added_on", 
            "last_modified_on",
            "quote_revision_count"
            ]]


        # Save DataFrame to Report.xlsx to attach on email.
        outdir = "./temp"
        outfilename = "Lead_DSR_GloboTransPro.xlsx"
        if not os.path.exists(outdir):
            os.mkdir(outdir)
        filefullname = os.path.join(outdir, outfilename)
        tb_leadmaster_xlsx.to_excel(filefullname, sheet_name="result", index=False)

        return (tb_leadmaster, filefullname, outfilename)

    # alert_for_taskmaster Method return pandas.DataFrame for task created or modified in past 6 minutes.
    @staticmethod
    def alert_for_taskmaster():
        engine = sqlal.create_engine("mysql+pymysql://"+ Username +":"+ Password +"@"+ Hostname +"/"+ DatabaseName)

        #create `task_master` DataFrame from SQL Table
        tb_taskmaster = pd.read_sql_table(
            "task_master", 
            engine
            )
        #create `emp_master` DataFrame from SQL Table
        tb_empmaster = pd.read_sql_table(
            "employee_master", 
            engine, 
            columns={"employee_id", "employee_name", "employee_designation", "email_address"}
            )
        #create `predefined_master` DataFrame from SQL Table
        tb_predefinedmaster = pd.read_sql_table(
            "predefined_master",
            engine,
            columns={"predefined_id","name"}
        )

        
        tb_taskmaster["created_ago"] = (datetime.datetime.now() - tb_taskmaster["created_date"])/timedelta(hours=1)
        tb_taskmaster["modified_ago"] = (datetime.datetime.now() - tb_taskmaster["modified_date"])/timedelta(hours=1)

        # select rows in last 6 minutes & yes is_delete as 'N'
        tb_taskmaster = tb_taskmaster[ 
            (tb_taskmaster["is_delete"] == 'N') & (
                (tb_taskmaster["modified_ago"] < 0.1) | 
                (tb_taskmaster["created_ago"] < 0.1)
            )
            ]

        # task_status_id => merge
        tb_taskmaster = pd.merge( tb_taskmaster, tb_predefinedmaster[["predefined_id","name"]], how="left", left_on="task_status_id", right_on="predefined_id")
        # renaming master name column as status code
        tb_taskmaster = tb_taskmaster.rename(columns={
            "name":"status_code"
        })

        # assigned_by_id => merge
        tb_taskmaster = pd.merge( tb_taskmaster, tb_empmaster[["employee_id","employee_name", "email_address"]], how="left", left_on="assigned_by_id", right_on="employee_id")
        #renaming few column for easy recognisation
        tb_taskmaster = tb_taskmaster.rename(columns={
            "employee_id":"assignee_id",
            "employee_name":"assignee_name", 
            "employee_designation":"assignee_desgn",
            "email_address":"assignee_email_addr"
            })
        
        # assigned_empoyee_id => merge
        tb_taskmaster = pd.merge( tb_taskmaster, tb_empmaster[["employee_id","employee_name", "email_address"]], how="left", left_on="assigned_empoyee_id", right_on="employee_id")
        #renaming few column for easy recognisation
        tb_taskmaster = tb_taskmaster.rename(columns={
            "employee_id":"assignedto_id",
            "employee_name":"assignedto_name", 
            "employee_designation":"assignedto_desgn",
            "email_address":"assignedto_email_addr"
            })

        # Replace empty cells with `-`
        tb_taskmaster = tb_taskmaster.fillna(value={"task_description":"Task Description is Empty."})

        return tb_taskmaster
