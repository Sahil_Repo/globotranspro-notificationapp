#!/usr/bin/python3
from services.databaseservice import DatabaseSets as ds_live
from email_templates import EmailTemplate_LeadDSR, EmailTemplate_LeaveAlert, EmailTemplate_TaskAlert
from services.notificationservice import notificationservice as ns
from dotenv import load_dotenv
import os
import pandas as pd

"""
This is service for GloboTrans Pro's Notification Service 
"""
class Endpoint:

    @staticmethod
    def leave_notification():
        tb_leavemaster = ds_live.alert_for_employeeleaves()
        if tb_leavemaster.empty:
            # Skipping mail as no leaves to report.
            print( "Skipping mail as no leaves to report." )
        else:   
            for indexcounter in tb_leavemaster.index:
                receiver_emails = []
                if( pd.notnull(tb_leavemaster["off_emp_email_address"][indexcounter]) and not os.getenv('DEV_MODE') ):
                    receiver_emails.append( tb_leavemaster["off_emp_email_address"][indexcounter] )
                if( pd.notnull(tb_leavemaster["mgr_emp_email_address"][indexcounter]) and not os.getenv('DEV_MODE') ):
                    receiver_emails.append( tb_leavemaster["mgr_emp_email_address"][indexcounter] )
                receiver_emails.append( os.getenv('CC_EMAIL') )

                (plain_text, html_body) = EmailTemplate_LeaveAlert.create_email_body( tb_leavemaster, indexcounter )
                ns.sendHTMLNotification(
                    "alerts.globo@globotranspro.com",
                    receiver_emails,
                    "Leave Notification from " + str( tb_leavemaster["off_emp_name"][indexcounter] ),
                    plain_text,
                    html_body
                    )
        print("---Program End---")

    @staticmethod
    def leaddsr_notification():
        ( tb_leadmaster, fpath, fname) = ds_live.alert_for_leadsDSR()

        if( tb_leadmaster.empty):
            # skipping leads email as nothing to report
            print("skipping leads email as nothing to report")
        else:
            receiver_emails = []
            receiver_emails.append( os.getenv('LEADSDSR_EMAIL') )
            receiver_emails.append( os.getenv('CC_EMAIL') )
            
            (plain_text, html_body) = EmailTemplate_LeadDSR.create_email_body(tb_leadmaster)

            # Send Email Notification
            ns.sendHTMLNotification(
                "alerts.globo@globotranspro.com",
                receiver_emails,
                "Lead DSR Notification from ERP GloboTrans Pro",
                plain_text,
                html_body,
                fpath,
                fname
                )
        print("---Program End---")

    @staticmethod
    def task_notification():
        tb_task = ds_live.alert_for_taskmaster()

        if tb_task.empty:
            # Skipping mail as no task to report.
            print( "Skipping mail as no tasks to report." )
        else:
            for indexcounter in tb_task.index:
                receiver_emails = []
                if( pd.notnull(tb_task["assignedto_email_addr"][indexcounter]) and not os.getenv('DEV_MODE') ):
                    receiver_emails.append( tb_task["assignedto_email_addr"][indexcounter] )
                if( pd.notnull(tb_task["assignee_email_addr"][indexcounter]) and not os.getenv('DEV_MODE') ):
                    receiver_emails.append( tb_task["assignee_email_addr"][indexcounter] )
                
                receiver_emails.append( os.getenv('CC_EMAIL') )

                (plain_text, html_body) = EmailTemplate_TaskAlert.create_email_body(tb_task, indexcounter)
                ns.sendHTMLNotification(
                    "alerts.globo@globotranspro.com",
                    receiver_emails,
                    "Task Notification for " + str( tb_task["assignedto_name"][indexcounter] ) + " from " + str( tb_task["assignee_name"][indexcounter] ),
                    plain_text,
                    html_body
                    )
        print("---Program End---")
